<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.yeswedev.bzh
 * @since             1.0.0
 * @package           Ywd_Megasearch
 *
 * @wordpress-plugin
 * Plugin Name:       YWD Megasearch
 * Plugin URI:        https://www.yeswedev.bzh
 * Description:       Add support of ACF fields to the WP search. Also, you can filter which CPTs you want in the search.
 * Version:           1.0.0
 * Author:            Yes We Dev
 * Author URI:        https://www.yeswedev.bzh
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ywd-megasearch
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'YWD_MEGASEARCH_VERSION', '1.0.0' );


register_activation_hook( __FILE__, 'activate_ywd_megasearch' );
register_deactivation_hook( __FILE__, 'deactivate_ywd_megasearch' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ywd-megasearch.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ywd_megasearch() {

	$plugin = new Ywd_Megasearch();
	$plugin->run();

}
run_ywd_megasearch();
