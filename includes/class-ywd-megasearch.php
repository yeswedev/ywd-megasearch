<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.yeswedev.bzh
 * @since      1.0.0
 *
 * @package    Ywd_Megasearch
 * @subpackage Ywd_Megasearch/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Ywd_Megasearch
 * @subpackage Ywd_Megasearch/includes
 * @author     Yes We Dev <kevin.f@yeswedev.bzh>
 */
class Ywd_Megasearch {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Ywd_Megasearch_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'YWD_MEGASEARCH_VERSION' ) ) {
			$this->version = YWD_MEGASEARCH_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'ywd-megasearch';

		$this->load_dependencies();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Ywd_Megasearch_Loader. Orchestrates the hooks of the plugin.
	 * - Ywd_Megasearch_Admin. Defines all hooks for the admin area.
	 * - Ywd_Megasearch_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-ywd-megasearch-loader.php';

		$this->loader = new Ywd_Megasearch_Loader();

	}


	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

        add_filter( 'pre_get_posts', ['Ywd_Megasearch', 'addCpts'] );
        add_filter('posts_join', ['Ywd_Megasearch', 'postsJoin']);
        add_filter('posts_where', ['Ywd_Megasearch', 'postsWhere']);
        add_filter('posts_distinct', ['Ywd_Megasearch', 'postsDistinct']);

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Ywd_Megasearch_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

    /**
     * Add CPTs to the query.
     *
     * @since     1.0.0
     * @return    $query    The query.
     */
    public static function addCpts($query) {
        if ( $query->is_search ) {
            $query->set( 'post_type', array( 'post', 'page', 'actualites', 'portrait' ) );
        }

        return $query;
    }

    /**
     * Join posts and metadatas.
     *
     * @since     1.0.0
     * @return    $join    Query's join.
     */
    public static function postsJoin($join)
    {
        global $wpdb;

        if ( is_search() ) {
            $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        }
        return $join;
    }

    /**
     * Add where to query.
     *
     * @since     1.0.0
     * @return    $where    Query's where clausure.
     */
    public static function postsWhere($where)
    {
        global $pagenow, $wpdb;
        if ( is_search() ) {
            $where = preg_replace(
                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
        }

        return $where;
    }

    /**
     * Distinct posts.
     *
     * @since     1.0.0
     * @return    $where    Query's where clausure but distinct.
     */
    public static function postsDistinct($where)
    {
        global $wpdb;
        if ( is_search() ) {
            return "DISTINCT";
        }
        return $where;
    }
}
